from os.path import join as path_join, dirname
from setuptools import setup, find_packages


version = '0.1'
README = path_join(dirname(__file__), 'README.rst')
long_description = open(README).read()
setup(
    name='fiad',
    version=version,
    description=('Fault Injection Advisor'),
    long_description=long_description,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
    ],
    keywords='fault-injection infrastructure-as-code Ansible',
    author='Lukas Pirl, Lena Feinbube',
    author_email='fiad@lukas-pirl.de',
    url='https://gitlab.com/lpirl/fiad',
    download_url='https://gitlab.com/lpirl/fiad/-/archive/master/fiad-master.tar.bz2',
    package_dir={'': 'src'},
    packages=find_packages('src', exclude=['test']),
    entry_points={
        'console_scripts': ['fiad = fiad:standalone']
    },
    install_requires=[
        'ansible==2.0.1.0',
        'coloredlogs==10.0',
    ],
    extras_require={
        'doc': [
            'pylint==2.3.1',
            'Sphinx'
        ]
    },
)
