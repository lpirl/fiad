"""
Contains base classes for tests.
"""

import unittest
import sys
from os.path import join as path_join
import abc

from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager

from fiad.cli import Cli
from fiad.playbook import Playbook


class BaseTest(unittest.TestCase, metaclass=abc.ABCMeta):
    """
    Provides some helpers for all our tests.
    """

    @staticmethod
    def load_playbook(path_components):
        """
        Returns the Playbook specified by ``path_components``
        (e.g. ``("test", "ansible", "test_playbook.yml")``).
        """
        loader = DataLoader()
        variable_manager = VariableManager()
        path = path_join(*path_components)
        return Playbook.load(path, variable_manager=variable_manager,
                             loader=loader)
