"""
See :py:class:`PlaybookTest.
"""

from test import BaseTest


class PlaybookTest(BaseTest):
    """
    Tests for (our implementation of the) :py:class:`Playbook`.

    Usually test by loading a known example Playbook file and checking
    some known/expected samples/results.
    """

    def setUp(self):
        self.playbook = self.load_playbook(
            ("src", "test", "playbooks", "ansible-ec2-hadoop-cluster",
             "ansible", "create-instances.yml")
        )

    def test_get_tasks(self):
        """
        Tests the method :py:func:`get_tasks` of Playbook.
        """
        tasks = tuple(self.playbook.get_tasks())
        non_meta_tasks = tuple(t for t in tasks if t.action != "meta")
        self.assertEqual(len(non_meta_tasks), 22)

        task_names = tuple(t.name for t in tasks)

        # sample tests:
        self.assertIn("Adding hosts to custom inventory", task_names)
        self.assertIn("Create the VPC", task_names)

    def test_get_modules(self):
        """
        Tests the method :py:func:`get_tasks` of Playbook.
        """
        module_names = self.playbook.get_module_names()
        self.assertIn("ec2_vpc", module_names)
        self.assertIn("ec2_group", module_names)
        self.assertIn("ec2", module_names)
