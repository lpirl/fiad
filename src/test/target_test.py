"""
Tests for targets.

(Once they grow more in numbers, this module has to be split up most
likely.)
"""

from test import BaseTest

from fiad.models.targets.operating_system_targets import WindowsTarget


class WindowsTargetTest(BaseTest):
    """
    Tests for :py:class:`WindowsTarget`.
    """

    def setUp(self):
        self.windows_playbook = self.load_playbook(
            ("src", "test", "playbooks", "ansible-examples", "windows",
             "run-powershell.yml")
        )
        self.non_windows_playbook = self.load_playbook(
            ("src", "test", "playbooks", "ansible-examples", "mongodb",
             "site.yml")
        )
        self.target = WindowsTarget()

    def test_detect(self):
        """
        Tests if WindowsTarget detects itself correctly (and also if not).
        """
        # sample tests:
        self.assertTrue(self.target.detect(self.windows_playbook))
        self.assertFalse(self.target.detect(self.non_windows_playbook))

# TODO
