This directory contains some publicly accessible Git repositories with
some Ansible Playbooks for us to run our tests.

Those repositories are included as a Git subtrees.

``git subtree`` cheat sheet
---------------------------

See also `this tutorial <https://blogs.atlassian.com/2013/05/alternatives-to-git-submodule-git-subtree/>`__.

Adding a repository::

  cd <repository root>
  git remote add -f <repository-name> <repository-url>
  git subtree add --prefix src/test/ansible/<repository-name> <repository-name> master --squash

Updating a subtree::

  cd <repository root>
  git subtree pull --prefix src/test/ansible/<name> <repository-name> master --squash
