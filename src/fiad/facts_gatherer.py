"""
See class :py:class:`FactsGatherer`.
"""

import logging
from functools import lru_cache
from subprocess import check_output, CalledProcessError
from shlex import quote as shell_quote
from tempfile import mkdtemp
from shutil import rmtree
from os import listdir
from os.path import join as path_join, isdir
from json import load as load_json, JSONDecodeError

from fiad.host_facts import HostFacts


class FactsGatherer:
    """
    Gathers facts about hosts.
    """

    @classmethod
    def parse_ansible_output(cls, facts_tmp_dir):
        """
        Parses all files in ``facts_tmp_dir``, expecting them to be JSON
        as put out by `ansible ... -m setup --tree ...` and returns
        dictionary mapping hosts (i.e. file names in ``facts_tmp_dir``)
        to their facts dictionary.
        """
        all_facts = dict()
        for dir_entry in listdir(facts_tmp_dir):
            path = path_join(facts_tmp_dir, dir_entry)
            if isdir(path):
                continue
            try:
                with open(path) as path_fp:
                    facts = load_json(path_fp).get("ansible_facts", False)
                    if facts:
                        all_facts[dir_entry] = HostFacts(facts)
            except JSONDecodeError:
                logging.error("Could not load JSON from '%s'.", path)
        return all_facts

    @classmethod
    @lru_cache(maxsize=100)
    def gather_facts_for_host(cls, host):
        """
        Tries to gather facts (Ansible module "setup") for host
        and returns dict of facts on success.

        More precisely, ``host`` is a string as Ansible understands them.
        Consequently, it can imply other hosts (e.g. 'all').

        We let Ansible care about the resolution of that host (string)
        to actual hosts.
        """
        facts_tmp_dir = mkdtemp(prefix="fiad-ansible-facts-")
        host_quoted = shell_quote(host)
        try:
            # Hack to run ansible outside a possible virtualenv.
            # (Otherwise, in a Python 3 virtualenv, Ansible would run
            # with Python 3 as well.)
            #
            # Alternatives would be to spawn a login shell (but
            # this would loose possible custom environment
            # variables) or remove entries containing "virtualenv" from
            # the environment variable PATH (but this can probably not
            # reliably be done without risking to remove valid entries).
            ansible_cmd = "$(whereis ansible | cut -d' ' -f2)"

            check_output('%s %s -m setup --tree %s' % (ansible_cmd,
                                                       host_quoted,
                                                       facts_tmp_dir),
                         shell=True)

        except CalledProcessError:
            logging.warning(
                "Gathering facts for '%s' returned an error.",
                host
            )
        else:
            logging.debug("Gathered facts for host '%s'.", host)

        facts = cls.parse_ansible_output(facts_tmp_dir)
        rmtree(facts_tmp_dir)
        return facts

    @classmethod
    @lru_cache(maxsize=100)
    def gather_facts_for_hosts(cls, hosts):
        """
        Tries to gather facts (Ansible module "setup") for all hosts
        and returns successfully gathered facts as a dict ({host: facts}).
        """
        facts = dict()
        for host in hosts:
            facts.update(cls.gather_facts_for_host(host))
        return facts
