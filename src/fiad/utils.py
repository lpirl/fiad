"""
This module contains helpers that do not fit into any other module and
that are too few to get their own module.
"""

from re import compile as re_compile, IGNORECASE


def re_compile_ci(regexp):
    """
    Case-insensitively compile a regular expression.
    """
    return re_compile(regexp, IGNORECASE)
