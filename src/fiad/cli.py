"""
Contains the main CLI application and the top-level coordination of the
program.

This module can also be called as a standalone script.
"""

# encoding: UTF-8

from sys import argv
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
from itertools import product
from os import walk
from os.path import isdir, join as path_join

from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from yaml import load as load_yaml, YAMLError, Loader
import coloredlogs

from fiad.models import Registry
from fiad.playbook import Playbook


class Cli:
    """
    Implements top-level coordination and interaction as CLI.

    After initialization, all you need is :py:func:`run()`.
    """

    def __init__(self):
        """
        Finds/loads/initializes everything needed for operation.
        """
        # enabling debugging is the first thing we (might) have to do:
        if '-d' in argv or '--debug' in argv:
            logging.getLogger().setLevel(logging.DEBUG)

        logging.debug("initializing CLI")

        self.targets = None
        """
        The set of all known targets.
        """

        self.detected_targets = None
        """
        The set of all (directly) detected targets.
        """

        self.implied_targets = None
        """
        The set of all implied targets (through the detected ones).
        """

        self.injectors = None
        """
        The set of all known injectors.
        """

        self._init_arg_parser()
        self._init_logging()
        self._set_up_injectors()
        self._set_up_targets()
        self.args = None
        self.playbooks = None

    def _init_arg_parser(self):
        self.arg_parser = ArgumentParser(
            description="Fault Injection Advisor - " +
            "tries to find suitable fault injectors and corresponding " +
            "targets by looking at Ansible Playbooks.",
            formatter_class=ArgumentDefaultsHelpFormatter,
        )

    def _init_logging(self):
        coloredlogs.install(fmt='%(levelname)s %(message)s')

        logging.getLogger().name = "fiad"
        self.arg_parser.add_argument('-d', '--debug', action='store_true',
                                     default=False,
                                     help='turn on debug messages')
        self.arg_parser.add_argument('-v', '--verbose', action='store_true',
                                     default=False,
                                     help='turn on verbose messages')
        self.arg_parser.add_argument('-q', '--quiet', action='store_true',
                                     default=False,
                                     help='silence warnings')
        self.arg_parser.add_argument('-r', '--recursive',
                                     action='store_true', default=False,
                                     help=('try to identify and load '
                                           'Playbooks recursively'))
        self.arg_parser.add_argument('-f', '--facts',
                                     action='store_true', default=False,
                                     help=('try to CONNECT TO HOSTS, '
                                           'run Ansible modules "setup" '
                                           'and consider gathered facts '
                                           'for injector advices'))
        self.arg_parser.add_argument('playbooks', nargs='+',
                                     metavar="playbook",
                                     help=('Ansible Playbook to analyze '
                                           '(or a directory, if using -r)'))

    def _set_up(self, classes):
        """
        Initializes and sets up all classes found in the list
        ``classes`` and returns initialized instances.
        """
        for cls in classes:
            logging.debug("initializing and setting up %s", cls.__name__)
            cls.set_up(self.arg_parser)
        return classes

    def _set_up_injectors(self):
        """
        Initializes and sets up all injector classes from the
        corresponding module.
        """
        self.injectors = self._set_up(Registry.injectors)

    def _set_up_targets(self):
        """
        Initializes and sets up all target classes from the
        corresponding module.
        """
        self.targets = self._set_up(Registry.targets)

    def run(self):
        """
        This kicks off the actual operation (i.e. use the users' args,
        options and sub commands to server the request).
        """
        logging.debug("starting to run")

        self._parse_args()
        playbook_paths = self._find_playbooks()
        self._load_playbooks(playbook_paths)
        self._detect_targets()
        self._imply_targets()
        self._match_injectors()

    def _parse_args(self):
        logging.debug("parsing command line arguments")

        # display help per default:
        if len(argv) == 1:
            argv.append("-h")

        args = self.arg_parser.parse_args()
        self.args = args

        if args.verbose and args.quiet:
            print("Options -v and -q conflict... exiting")
            exit(42)

        if args.debug and args.quiet:
            print("Options -d and -q conflict... exiting")
            exit(42)

        if args.verbose:
            logging.getLogger().setLevel(logging.INFO)

        if args.debug:
            logging.getLogger().setLevel(logging.DEBUG)

        if args.quiet:
            logging.getLogger().setLevel(logging.FATAL)

    def _find_playbooks(self):
        """
        Returns a list of paths to Playbooks, by examining CLI-specified
        paths under consideration of the switch ``-r``/``--recursive``.
        """

        if not self.args.recursive:
            return self.args.playbooks

        playbook_paths = []
        for path in self.args.playbooks:
            if isdir(path):
                logging.debug(
                    "searching for Playbooks in specified directory '%s'",
                    path
                )
                # for directories, we try to detect Playbooks
                playbook_paths.extend(self._playbook_paths_from_dir(path))
            else:
                # for anything else than directories, we expect the CLI
                # argument to be a Playbook
                logging.debug(
                    "using specified file '%s' as path to Playbook",
                    path
                )
                playbook_paths.append(path)

        return playbook_paths

    @staticmethod
    def _playbook_paths_from_dir(dir_path):
        """
        Returns a list of paths to Playbooks that were found within the
        directory ``dir_path`` (recursively).
        """
        playbook_paths = []
        for root_path, _, file_names in walk(dir_path):
            for file_name in file_names:
                file_path = path_join(root_path, file_name)
                data = None
                with open(file_path, "r") as open_file:
                    try:
                        data = load_yaml(open_file, Loader=Loader)
                    except (YAMLError, UnicodeError):
                        continue

                # ignore empty YAMLs etc
                if not data:
                    continue

                # Is this assumption too strong?
                if not isinstance(data, list):
                    continue

                # 'hosts' is what every Playbook needs
                if not 'hosts' in data[0]:
                    continue

                # Do we need any more checks here? tasks, roles, ...?

                logging.info("Detected '%s' as Playbook", file_path)
                playbook_paths.append(file_path)

        return playbook_paths

    def _load_playbooks(self, playbook_paths):
        """
        Loads all Playbooks.
        """

        loader = DataLoader()
        variable_manager = VariableManager()
        self.playbooks = []

        for file_path in playbook_paths:
            try:
                playbook = Playbook.load(file_path,
                                         variable_manager=variable_manager,
                                         loader=loader)
            except Exception as exception:
                logging.warning(
                    ("Could not load Playbook from file '%s': %s"),
                    file_path, exception,
                )
                continue

            else:
                logging.debug("Plays in %s: %u", playbook,
                              len(playbook.get_plays()))
                if self.args.facts:
                    playbook.gather_facts()
                else:
                    logging.info(
                        "If you enable the gathering of facts from the "
                        "target hosts (option -f), results might be "
                        "more accurate."
                    )
                self.playbooks.append(playbook)

    def _detect_targets(self):
        """
        Feeds all Playbooks into all targets and stores detected targets
        in ``self.detected_targets``.
        """
        logging.debug("Detecting targets")
        self.detected_targets = set()
        combinations = product(self.playbooks, self.targets)
        for playbook, target in combinations:
            if target.detect(playbook):
                logging.info("%s detected in %s", target, playbook)
                self.detected_targets.add(target)

    def _imply_targets(self):
        """
        Fills ``self.implied_targets``.
        """
        self.implied_targets = set()
        for target in self.detected_targets:
            self.implied_targets.update(target.get_implied_targets())

    def _match_injectors(self):
        """
        Feeds the set of all targets (i.e., directly detected and
        implied) into all injectors to let them check their
        applicability.
        """
        logging.debug("matching injectors with targets %s",
                      self.detected_targets)

        for injector in self.injectors:
            result = injector.match(self.detected_and_implied_targets)
            result_string = "%s injector: %s" % (injector, result)
            if result:
                print(result_string)

    @property
    def detected_and_implied_targets(self):
        """
        Insanely complex function returning ``self.detected_targets`` and
        ``self.implied_targets`` combined into a single set.
        """
        return self.detected_targets.union(self.implied_targets)
