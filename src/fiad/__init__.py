"""
Root package, which holds all implementations for FIAD (except test).

This module itself contains a helper to provide a common entry
point for standalone CLI usage (executable in the repository root) and
the pip-installed command.
"""

from fiad.cli import Cli

def standalone():
    """
    Creates CLI handler and hands over to it.
    """
    Cli().run()
