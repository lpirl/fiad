"""
See :py:class:`HostFacts`.
"""


class HostFacts(dict):
    """
    A dict-like class with some helpers for easier access to facts dicts.
    """

    def contains(self, other):
        """
        Returns ``True`` if all key-value pairs of ``other`` are also
        within ``self``. ``False`` otherwise.
        """
        for key, value in other.items():
            if key not in self:
                return False
            if value != self[key]:
                return False
            return True
