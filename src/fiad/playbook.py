"""
See :py:class:`Playbook`.
"""

import logging
from functools import lru_cache
from os.path import basename
from ansible.playbook import Playbook as AnsiblePlaybook

from fiad.facts_gatherer import FactsGatherer


class Playbook(AnsiblePlaybook):
    """
    A wrapper class around Ansible's Playbook that adds some helpers.
    """

    # see: http://docs.ansible.com/ansible/list_of_packaging_modules.html
    # for now, only modules that explicitly install a specified package
    # are considered (e.g. by specifying "name", "path", etc.)
    PACKAGE_MODULES = {
        "bower": ("name",),
        "cpanm": ("name", "okg",),
        "easy_install": ("name",),
        "gem": ("name",),
        "npm": ("name",),
        "pear": ("name",),
        "pip": ("name",),
        "apk": ("name",),
        "apt": ("name", "pkg", "package",),
        "apt_rpm": ("pkg",),
        "dnf": ("name",),
        "homebrew": ("name",),
        "homebrew_cask": ("name",),
        "macports": ("name",),
        "openbsd_pkg": ("name",),
        "opkg": ("name",),
        "package": ("name",),
        "pacman": ("name",),
        "pkg5": ("name",),
        "pkgin": ("name",),
        "pkgng": ("name",),
        "pkgutil": ("name",),
        "portage": ("package",),
        "portinstall": ("name",),
        "slackpkg": ("name",),
        "svr4pkg": ("name",),
        "swdepot": ("name",),
        "urpmi": ("pkg",),
        "yum": ("name", "pkg",),
        "zypper": ("name", "pkg",)
    }

    def __init__(self, *args, **kwargs):
        super(Playbook, self).__init__(*args, **kwargs)
        self.facts = None

    @staticmethod
    def load(file_name, variable_manager=None, loader=None):
        """
        Overrides the super class' method, so we can instantiate our
        own Playbook class.
        """
        playbook = Playbook(loader=loader)

        # We are a subclass *and* we have no control over the parent
        # class. We hence "have to":
        # pylint: disable=protected-access
        playbook._load_playbook_data(file_name=file_name,
                                     variable_manager=variable_manager)
        return playbook

    @lru_cache(maxsize=2)
    def get_hosts(self):
        """
        Returns an iterable containing all hosts referenced in this Playbook
        (literally, not expanded).
        """
        return (host for play in self.get_plays() for host in play.hosts)

    @lru_cache(maxsize=2)
    def get_tasks(self):
        """
        Returns a set containing all tasks of this Playbook.
        """
        tasks = set()
        for play in self.get_plays():
            for block in play.compile():
                for task in block.block:
                    tasks.add(task)
        logging.debug("tasks in %s: %s", self, tasks)
        return tasks

    @lru_cache(maxsize=2)
    def get_module_names(self):
        """
        Returns the set of all (locally and remotely used) modules
        that are references in this Playbook.
        """
        module_names = set(task.action for task in self.get_tasks())
        logging.debug("modules used in %s: %s", self, module_names)
        return module_names

    @lru_cache(maxsize=2)
    def get_command_names(self):
        """
        Returns the set of all commands that are to be called when
        executing this Playbook.

        Currently, this only includes all first arguments to the
        "command" and the "shell" module.
        """
        command_names = set()
        for task in self.get_tasks():

            if task.action not in ("command", "shell"):
                continue

            command_line = task.args['_raw_params']
            bits = command_line.split()

            # ignore environment variables before command:
            while "=" in bits[0]:
                bits.pop(0)

            command = basename(bits[0])
            command_names.add(command)

        logging.debug("commands used in %s: %s", self, command_names)
        return command_names

    @lru_cache(maxsize=2)
    def get_package_names(self):
        """
        Returns a set containing all installed packages of this Playbook.
        """
        packages = set()

        modules_of_interest = self.PACKAGE_MODULES.keys()
        for task in self.get_tasks():

            action_name = task.action

            if action_name not in modules_of_interest:
                continue

            get_arg = task.args.get

            if get_arg("state", "present") == "absent":
                continue

            package_name = None
            for arg_of_interest in self.PACKAGE_MODULES[action_name]:
                package_name = get_arg(arg_of_interest, None)
                if package_name:
                    package_name = package_name.lower()
                    packages.add(package_name)
                    break

        logging.debug("packages installed via %s: %s", self, packages)
        return packages

    def gather_facts(self):
        """
        Tries to gather facts from all hosts of this Playbooks.

        As opposed to most of the other attributes, the facts have to be
        gathered explicitly.
        This is because the Playbook does (and should) not know if facts
        gathering is enabled.
        """
        hosts = self.get_hosts()
        self.facts = FactsGatherer.gather_facts_for_hosts(hosts)

    def get_facts(self):
        """
        Returns (possibly) gathered facts for all hosts of this Playbook.

        See also :func:``get_facts``.
        """
        return self.facts

    def __str__(self):
        return "%s '%s'" % (self.__class__.__name__, self._file_name)
