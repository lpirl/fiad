"""
See :py:class:`LFIInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector, InjectionTime
from fiad.models import Registry
from fiad.models.targets.operating_system_targets import (
    WindowsTarget, LinuxTarget, SolarisTarget, UnixLikeOperatingSystemTarget
)
from fiad.models.targets.artifact_targets import DLLTarget
from fiad.models.targets.platform_targets import Sparc64Target, X86Target


@Registry.register_injector
class LFIInjector(AbstractBaseInjector):
    """
    Represents the LFI (Library Fault Injector).
    """

    FRIENDLY_NAME = "LFI"

    PUBLICATION_BIBTEX = """@inproceedings{marinescu2009lfi,
      title={LFI: A practical and general library-level fault injector},
      author={Marinescu, Paul D and Candea, George},
      booktitle={Dependable Systems \\& Networks, 2009. DSN'09.
                 IEEE/IFIP International Conference on},
      pages={379--388},
      year={2009},
      organization={IEEE}
    }"""

    WEB_SITE_URL = "http://lfi.epfl.ch/"

    INJECTION_TIME = InjectionTime.linktime

    REQUIRED_TARGETS_ALL = {UnixLikeOperatingSystemTarget, DLLTarget} # APITarget

    REQUIRED_TARGETS_ANY = {WindowsTarget, LinuxTarget, SolarisTarget}

    REQUIRED_PLATFORM_TARGETS = {Sparc64Target, X86Target}
    # Warning: Not yet implemented (TODO).
