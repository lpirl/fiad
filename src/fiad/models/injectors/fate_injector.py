"""
See :py:class:`FATEInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector, FaultCoverage, TriggerMechanism
from fiad.models import Registry
from fiad.models.targets.virtual_machine_targets import CloudVirtualMachineTarget
from fiad.models.targets.language_targets import JavaTarget


@Registry.register_injector
class FATEInjector(AbstractBaseInjector):
    """
    FATE aims at a high coverage of failure scenarios in cloud systems,
    including multi-fault ones.

    It requires the implementation of an interface.
    """

    FRIENDLY_NAME = "FATE"

    PUBLICATION_BIBTEX = """@inproceedings{gunawi2011fate,
        title={FATE and DESTINI: A framework for cloud recovery testing},
        author={Gunawi, Haryadi S and Do, Thanh and Joshi, Pallavi and
                Alvaro, Peter and Hellerstein, Joseph M and
                Arpaci-Dusseau, Andrea C and Arpaci-Dusseau, Remzi H and
                Sen, Koushik and Borthakur, Dhruba},
        booktitle={Proceedings of NSDI’11: 8th USENIX Symposium on
                   Networked Systems Design and Implementation},
        pages={239},
        year={2011}
    }"""

    WEBSITE_URL = "http://cloudfail.cs.berkeley.edu/download.html"

    REQUIRED_TARGETS_ALL = {CloudVirtualMachineTarget, JavaTarget}

    FAULT_COVERAGE = FaultCoverage.explorative_exhaustive

    TRIGGER_MECHANISM = TriggerMechanism.configurable
