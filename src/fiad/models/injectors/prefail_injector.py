"""
See py:class:`PrefailInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector, FaultCoverage
from fiad.models import Registry
from fiad.models.targets.virtual_machine_targets import VirtualMachineTarget


@Registry.register_injector
class PrefailInjector(AbstractBaseInjector):
    """
    PREFAIL is an extension of the FATE fault injector with further
    abstractions such as fault injection points and tasks, which are
    profiled.
    The underlying FI engine is that of FATE.
    """

    FRIENDLY_NAME = "PREFAIL"

    PUBLICATION_BIBTEX = """@inproceedings{joshi2011prefail,
        title={PREFAIL: A programmable tool for multiple-failure
               injection},
        author={Joshi, Pallavi and Gunawi, Haryadi S and Sen, Koushik},
        booktitle={ACM SIGPLAN Notices},
        volume={46},
        number={10},
        pages={171--188},
        year={2011},
        organization={ACM}
    }"""

    WEBSITE_URL = "https://sourceforge.net/projects/prefail/"

    REQUIRED_TARGETS_ALL = {VirtualMachineTarget}

    FAULT_COVERAGE = FaultCoverage.explorative_exhaustive
