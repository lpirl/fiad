# encoding: utf8

"""
This module contains the base implementation for all injectors.

Sub modules contain the injectors themselves.
"""

from abc import ABCMeta, abstractmethod
import logging
from enum import Enum

from fiad.models import AbstractBaseModel


class InjectionTime(Enum):
    """
    Holds possible values for the time (in the life time of software) a
    injector can operate.
    """
    compiletime = 1
    linktime = 2
    runtime = 3


class TriggerMechanism(Enum):
    """
    Holds different potential trigger mechanisms of when a fault is
    injected.
    """
    manual = 1
    state_based = 2
    time_based = 3
    rate_based = 4
    configurable = 5
    hardware_event_based = 6


class FaultCoverage(Enum):
    """
    Holds values which describe to what extent the potential fault
    space is covered.
    """
    random = 1
    exhaustive = 2
    explorative_exhaustive = 3


class AbstractBaseInjector(AbstractBaseModel, metaclass=ABCMeta):
    """
    All injectors must inherit from this base class.
    """

    PUBLICATION_BIBTEX = None
    """
    BibTex of the publication that mentioned the injector first.
    """

    WEB_SITE_URL = None
    """
    Web site of this injector.
    """

    REQUIRED_TARGETS_ALL = None
    """
    A set of targets that must all be present in order to make the
    default implementation of ``match`` return true.

    Conflicts with ``REQUIRED_TARGETS_ANY`` not being empty.
    """

    REQUIRED_TARGETS_ANY = None
    """
    A set of targets where one or more must be present in order to make
    the default implementation of ``match`` return true.

    Conflicts with ``REQUIRED_TARGETS_ALL`` not being empty.
    """

    REQUIRED_PLATFORM_TARGETS = None
    """
    A set of supported target hardware platforms.

    .. warning::

        Not yet implemented.

        (But when defining injectors, required platforms – if any –
        might be specified already for future consideration/evaluation.)

    """
    # LF: Does it make sense to add all CPU targets here by default?
    # Most fault injectors can be compiled onto most HW platforms,
    # this variable only matters for tools using HW-specific features.
    # LP: Can't we treat platform requirements like all other targets?
    # Your scenario would only matter for injectors which require no
    # other specific target, no? Do such injectors exist? And if so,
    # would it be make sense to suggest such to users?

    INJECTION_TIME = None
    """ see :py:class:`InjectionTime` """

    TRIGGER_MECHANISMS = None
    """ see :py:class:`TriggerMechanism` """

    FAULT_COVERAGE = None
    """ see :py:class:`FaultCoverage` """

    @classmethod
    def _evaluate_required_targets_all(cls, targets):
        """
        Evaluates ``REQUIRED_TARGETS_ALL``.

        Returns (``overlap``, ``message``) where ``overlap`` is either:
        * ``None`` (i.e. ``REQUIRED_TARGETS_ALL`` is ``None``)
        * ``REQUIRED_TARGETS_ALL`` (i.e. all targets present) or
        * an empty set (i.e. not all targets present);
        and ``message`` is a message regarding the overlap.
        """
        if cls.REQUIRED_TARGETS_ALL is None:
            return (None, None)
        if not cls.REQUIRED_TARGETS_ALL.issubset(targets):
            return (set(), None)
        overlap = cls.REQUIRED_TARGETS_ALL
        message = ""
        if len(overlap) > 1:
            message += "jointly "
        names = ", ".join(t.verbose_str() for t in overlap)
        message += "with %s" % names
        return (overlap, message)

    @classmethod
    def _evaluate_required_targets_any(cls, targets):
        """
        Evaluates ``REQUIRED_TARGETS_ANY``.

        Returns (``overlap``, ``message``) where ``overlap`` is either:
        * ``None`` (i.e. ``REQUIRED_TARGETS_ANY`` is ``None``) or
        * the set of required *and* available targets;
        and ``message`` is a message regarding the overlap.
        """
        if cls.REQUIRED_TARGETS_ANY is None:
            return (None, None)
        overlap = cls.REQUIRED_TARGETS_ANY.intersection(targets)
        message = "with "
        names = ", ".join(t.verbose_str() for t in overlap)
        if len(overlap) > 1:
            message += "any of "
        message += names
        return (overlap, message)

    @classmethod
    def _evaluate_required_targets(cls, targets):
        """
        Evaluates ``REQUIRED_TARGETS_ALL`` or ``REQUIRED_TARGETS_ANY``
        or both, depending on what's not ``None``.
        If both are not ``None``, the logic of both attributes will be
        ANDed.
        Returns a set of targets with all targets from
        ``REQUIRED_TARGETS_ALL`` and ``REQUIRED_TARGETS_ALL`` that are
        detected, as well as an optional textual comment regarding the
        aforementioned set.
        """
        overlap_all, message_all = cls._evaluate_required_targets_all(
            targets
        )
        overlap_any, message_any = cls._evaluate_required_targets_any(
            targets
        )

        if cls.REQUIRED_TARGETS_ALL and cls.REQUIRED_TARGETS_ANY:
            if not overlap_all and overlap_any:
                return (False, None)
            return (
                overlap_all.union(overlap_any),
                "works %s and %s" % (message_all, message_any)
            )

        if cls.REQUIRED_TARGETS_ALL:
            return (overlap_all, "works %s" % message_all)

        if cls.REQUIRED_TARGETS_ANY:
            return (overlap_any, "works %s" % message_any)

        raise RuntimeError(
            "%s: At least one of cls.REQUIRED_TARGETS_ALL " % cls + \
            "and cls.REQUIRED_TARGETS_ANY must be set. " + \
            "Override the method ``match`` to implement custom rules."
        )

    @classmethod
    def match(cls, targets):
        """
        Returns either ``None`` (i.e., unknown), ``True``, ``False``,
        or a custom string as message to the user (e.g., "maybe" or some
        special circumstances depending on the applicability of ``cls``
        on ``target``.
        """
        overlap, message = cls._evaluate_required_targets(targets)

        if overlap:
            return message

        return False
