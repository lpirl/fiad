"""
See :py:class:`IOFuzzInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector
from fiad.models import Registry
from fiad.models.targets.hypervisor_targets import HypervisorTarget


@Registry.register_injector
class IOFuzzInjector(AbstractBaseInjector):
    """
    ``iofuzz`` sends random data (and therewith random commands) to an
    IO port.

    Since this is a rather generic injector, we limit the applicability
    to testing virtualization software.
    It could probably also be used to test IO devices, operating system
    kernels and everything else that implements an IO ABI.
    However, there are probably more specialized tools for specialized
    jobs.
    """

    PUBLICATION_BIBTEX = """@misc{ormandy2007empirical,
      title={An empirical study into the security exposure to hosts of
             hostile virtualized environments},
      author={Ormandy, Tavis},
      year={2007},
      publisher={Citeseer}
    }"""

    FRIENDLY_NAME = "iofuzz"

    REQUIRED_TARGETS_ALL = {HypervisorTarget}
