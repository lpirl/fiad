"""
See :py:class:`GPUInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector
from fiad.models import Registry
from fiad.models.targets.platform_targets import GPUTarget
from fiad.models.targets.language_targets import CUDATarget
from fiad.models.targets.artifact_targets import SymbolTarget


@Registry.register_injector
class GPUInjector(AbstractBaseInjector):
    """
    Represents the GPU-Qin injector.
    """

    FRIENDLY_NAME = "GPU-Qin"

    PUBLICATION_BIBTEX = """@inproceedings{fang2014gpu,
        title={GPU-Qin: A methodology for evaluating the error
               resilience of GPGPU applications},
        author={Fang, Bo and Pattabiraman, Karthik and Ripeanu, Matei
                and Gurumurthi, Sudhanva},
        booktitle={Performance Analysis of Systems and Software (ISPASS),
                   2014 IEEE International Symposium on},
        pages={221--230},
        year={2014},
        organization={IEEE}
    }"""

    WEB_SITE_URL = "https://github.com/DependableSystemsLab/GPU-Injector"

    REQUIRED_TARGETS_ALL = {GPUTarget, CUDATarget, SymbolTarget}
