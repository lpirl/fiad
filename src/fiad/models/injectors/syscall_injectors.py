"""
This module contains fault injectors that target (i.e., primarily and
directly) use system calls.
"""

from fiad.models.injectors import AbstractBaseInjector
from fiad.models import Registry
from fiad.models.targets.operating_system_targets import (
    LinuxTarget,
    UnixLikeOperatingSystemTarget
)


@Registry.register_injector
class TrinityInjector(AbstractBaseInjector):
    """
    Trinity calls syscalls at random, with random arguments.

    Not an original idea, and one that has been done many times before
    on Linux, and on other operating systems.
    Where Trinity differs is that the arguments it passes are not purely
    random.
    """

    FRIENDLY_NAME = "Trinity"
    WEB_SITE_URL = "http://codemonkey.org.uk/projects/trinity/"

    REQUIRED_TARGETS_ALL = {LinuxTarget}

    PUBLICATION_BIBTEX = """@InProceedings{jones06trinity,
        author    = {Jones, Dave},
        title     = {Trinity: A system call fuzzer},
        year      = {2006},
        url       = {https://github.com/kernelslacker/trinity},
    }"""


@Registry.register_injector
class BallistaInjector(AbstractBaseInjector):
    """
    The Ballista automated robustness testing approach probes software
    to see how effective it is at exception handling.

    The purpose of the Ballista OS Robustness Test Suite is to
    facilitate testing POSIX function robustness.

    (from https://users.ece.cmu.edu/~koopman/ballista/ and
    https://users.ece.cmu.edu/~koopman/ballista/ostest/index.html)
    """

    FRIENDLY_NAME = "Ballista"
    WEB_SITE_URL = ("https://users.ece.cmu.edu/~koopman/"
                    "ballista/ostest/index.html")

    REQUIRED_TARGETS_ALL = {UnixLikeOperatingSystemTarget}

    PUBLICATION_BIBTEX = """@InProceedings{kropp98automated,
        author       = {Kropp, Nathan P and Koopman, Philip J
                        and Siewiorek, Daniel P},
        title        = {Automated robustness testing of off-the-shelf
                        software components},
        booktitle    = {Digest of Papers. Twenty-Eighth Annual
                        International Symposium on Fault-Tolerant
                        Computing (Cat. No.98CB36224)},
        year         = {1998},
        pages        = {230--239},
        organization = {IEEE},
        date         = {1998-06},
        doi          = {10.1109/FTCS.1998.689474},
        eventtitle   = {Digest of Papers. Twenty-Eighth Annual
                        International Symposium on Fault-Tolerant
                        Computing (Cat. No.98CB36224)},
    }"""


@Registry.register_injector
class IKnowThisInjector(AbstractBaseInjector):
    """
    iknowthis is a fuzz testing framework for Linux system calls.
    It is designed to make system calls in random order, with random
    parameters in order to spot subtle kernel bugs.

    (from https://github.com/rgbkrk/iknowthis)
    """

    FRIENDLY_NAME = "iknowthis"
    WEB_SITE_URL = "https://github.com/rgbkrk/iknowthis"

    REQUIRED_TARGETS_ALL = {LinuxTarget}
