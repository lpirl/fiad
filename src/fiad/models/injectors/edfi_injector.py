"""
See :py:class`EDFIInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector
from fiad.models import Registry
from fiad.models.targets.artifact_targets import SourceCodeTarget
from fiad.models.targets.api_targets import APITarget
from fiad.models.targets.language_targets import LLVMTarget


@Registry.register_injector
class EDFIInjector(AbstractBaseInjector):
    """
    Represents the EDFI (Execution Driven Fault Injector).
    """

    FRIENDLY_NAME = "EDFI"

    PUBLICATION_BIBTEX = """@inproceedings{giuffrida2013edfi,
        title={EDFI: A dependable fault injection tool for dependability
               benchmarking experiments},
        author={Giuffrida, Cristiano and Kuijsten, Anton and Tanenbaum,
                Andrew S},
        booktitle={Dependable Computing (PRDC), 2013 IEEE 19th Pacific
                   Rim International Symposium on},
        pages={31--40},
        year={2013},
        organization={IEEE}
    }"""

    WEB_SITE_URL = "http://www.minix3.org/docs/conf/prdc-2013.pdf"

    REQUIRED_TARGETS_ALL = {SourceCodeTarget, APITarget}

    REQUIRED_TARGETS_ANY = {LLVMTarget}
