"""
See :py:class:`FINEInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector, InjectionTime
from fiad.models import Registry
from fiad.models.targets.operating_system_targets import (
    UnixLikeOperatingSystemTarget,
)


@Registry.register_injector
class FINEInjector(AbstractBaseInjector):
    """
    Represents the famous injector FINE, which injects hardware-induced
    software errors into the UNIX kernel and then monitors its behaviour.
    """

    FRIENDLY_NAME = "FINE"

    PUBLICATION_BIBTEX = """@article{kao1993fine,
        title={FINE: A fault injection and monitoring environment for
               tracing the UNIX system behavior under faults},
        author={Kao, Wei-Lun and Iyer, Ravishankar K and Tang, Dong},
        journal={Software Engineering, IEEE Transactions on},
        volume={19},
        number={11},
        pages={1105--1118},
        year={1993},
        publisher={IEEE}
    }"""

    INJECTION_TIME = InjectionTime.compiletime

    REQUIRED_TARGETS_ALL = {UnixLikeOperatingSystemTarget}
