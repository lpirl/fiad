"""
See :py:class:`OrchestraInjector`.
"""
import logging

from fiad.models.injectors import AbstractBaseInjector
#from fiad.models import Registry


# see class docstring on why this injector is not being registered
#@Registry.register_injector
class OrchestraInjector(AbstractBaseInjector):
    """
    Orchestra is a fault injection framework for testing dependability
    and timeliness of distributed protocols.

    We currently do not register this injector since it yet lacks the
    definition of required targets and/or an implementation for ``match``.
    """

    FRIENDLY_NAME = "Orchestra"

    PUBLICATION_BIBTEX = """@article{dawson1996orchestra,
        title={ORCHESTRA: A fault injection environment for distributed
               systems},
        author={Dawson, Scott and Jahanian, Farnam and Mitton, Todd},
        journal={Ann Arbor},
        volume={1001},
        pages={48109--2122},
        year={1996},
        publisher={Citeseer}
    }"""

    @classmethod
    def match(cls, _targets):
        # TODO
        logging.warning(
            "%s required targets not defined / ``match`` not implemented!",
            cls
        )
        return False
