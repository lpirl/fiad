"""
See :py:class:`ChaosMonkeyInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector
from fiad.models import Registry
from fiad.models.targets.virtual_machine_targets import EC2NodeTarget


@Registry.register_injector
class ChaosMonkeyInjector(AbstractBaseInjector):
    """
    Randomly crashes virtual machines on AWS w/ rate-control.
    """

    FRIENDLY_NAME = "ChaosMonkey"

    PUBLICATION_BIBTEX = """@article{tseitlin2013antifragile,
        title={The antifragile organization},
        author={Tseitlin, Ariel},
        journal={Communications of the ACM},
        volume={56},
        number={8},
        pages={40--44},
        year={2013},
        publisher={ACM}
    }"""

    WEB_SITE_URL = "https://github.com/Netflix/SimianArmy/wiki/Chaos-Monkey"

    REQUIRED_TARGETS_ALL = {EC2NodeTarget}
