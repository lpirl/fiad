"""
See :py:class:`CrashmeInjector`.
"""

from fiad.models import Registry
from fiad.models.injectors import AbstractBaseInjector
from fiad.models.targets.operating_system_targets import WindowsTarget


@Registry.register_injector
class CrashmeInjector(AbstractBaseInjector):
    """
    Represents the injector CRASHME.

    Crashme is a very simple program that tests the operating
    environment's robustness by invoking random data as if it were a
    procedure.
    """

    FRIENDLY_NAME = "crashme"

    WEB_SITE_URL = "http://people.delphiforums.com/gjc/crashme.html"

    REQUIRED_TARGETS_ALL = {WindowsTarget}
