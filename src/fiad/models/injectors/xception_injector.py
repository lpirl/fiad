"""
See :py:class:`XceptionInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector, TriggerMechanism, InjectionTime
from fiad.models import Registry
from fiad.models.targets.artifact_targets import BinaryTarget
from fiad.models.targets.language_targets import CTarget, AdaTarget
from fiad.models.targets.platform_targets import (Sparc64Target,
                                                  Power64Target,
                                                  X86Target)


@Registry.register_injector
class XceptionInjector(AbstractBaseInjector):
    """
    Represents the famous injector Xception.
    """

    FRIENDLY_NAME = "Xception"

    PUBLICATION_BIBTEX = """@article{carreira1998xception,
        title={Xception: A technique for the experimental evaluation of
               dependability in modern computers},
        author={Carreira, Jo{\\~a}o and Madeira, Henrique and Silva,
                Jo{\\~a}o Gabriel},
        journal={Software Engineering, IEEE Transactions on},
        volume={24},
        number={2},
        pages={125--136},
        year={1998},
        publisher={IEEE}
    }"""

    WEBSITE_URL = "http://www.criticalsoftware.com/en/products/p/xception"

    INJECTION_TIME = InjectionTime.runtime

    TRIGGER_MECHANISMS = {TriggerMechanism.time_based,
                          TriggerMechanism.hardware_event_based}

    REQUIRED_TARGETS_ALL = {BinaryTarget}

    REQUIRED_TARGETS_ANY = {CTarget, AdaTarget}

    REQUIRED_PLATFORM_TARGETS = {Sparc64Target, Power64Target, X86Target}
    # Warning: Not yet implemented (TODO).
