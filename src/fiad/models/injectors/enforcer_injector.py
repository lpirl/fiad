"""
See :py:class:`EnforcerInjector`.
"""

from fiad.models.injectors import AbstractBaseInjector
from fiad.models import Registry
from fiad.models.targets.language_targets import JavaTarget
from fiad.models.targets.artifact_targets import BytecodeTarget


@Registry.register_injector
class EnforcerInjector(AbstractBaseInjector):
    """
    Represents the Enforcer injector.

    Enforcer injects exceptions into Java bytecode of applications using
    the JUnit framework.
    """

    FRIENDLY_NAME = "Enforcer"

    PUBLICATION_BIBTEX = """@incollection{artho2006enforcer,
        title={Enforcer--efficient failure injection},
        author={Artho, Cyrille and Biere, Armin and Honiden, Shinichi},
        booktitle={FM 2006: Formal Methods},
        pages={412--427},
        year={2006},
        publisher={Springer}
    }"""

    WEB_SITE_URL = "https://staff.aist.go.jp/c.artho/enforcer/"

    REQUIRED_TARGETS_ALL = {JavaTarget, BytecodeTarget}
