"""
This module contains targets representing operating systems, as well as
abstract categories of those represented in the class hierarchy.
"""
import logging

from fiad.models import Registry
from fiad.models.targets import AbstractBaseTarget
from fiad.utils import re_compile_ci

from .api_targets import POSIXTarget


class OperatingSystemTarget(AbstractBaseTarget):
    """
    Represents an operating system.
    """

    FRIENDLY_NAME = "operating system"


@Registry.register_target
class WindowsTarget(OperatingSystemTarget):
    """
    Microsoft Windows Operating System.

    Due to the limited POSIX interoperability, we do not consider
    Windows as POSIX compliant.
    """

    FRIENDLY_NAME = "Windows"

    POWERSHELL_SCRIPT_RE = re_compile_ci(r'.*\.ps1($|\w)')

    @classmethod
    def detect(cls, playbook):

        # check for explicit use of win_* modules
        for module in playbook.get_module_names():
            if module.startswith("win_"):
                logging.info("the use of %s indicates the presence of %s",
                             module, cls)
                return True

        # check for PowerShell scripts
        for task in playbook.get_tasks():
            if task.action == "script":
                params = task.args.get('_raw_params', "")
                if cls.POWERSHELL_SCRIPT_RE.match(params):
                    logging.info("calling '%s' indicates the presence of %s",
                                 params, cls)
                    return True

        return False


class UnixLikeOperatingSystemTarget(OperatingSystemTarget, POSIXTarget):
    """
    Represents an Unix-like operating system.
    """

    FRIENDLY_NAME = "Unix-like operating system"

    # INDICATING_MODULES_ANY could be easily (and excessively) be filled
    #   by reading through
    #   http://docs.ansible.com/ansible/list_of_all_modules.html


@Registry.register_target
class LinuxTarget(UnixLikeOperatingSystemTarget):
    """
    Represents a GNU Linux operating system.
    """

    FRIENDLY_NAME = "Linux"

    INDICATING_MODULES_ANY = (
        "dnf",
        "yum",
        "debconf",
        "capabilities",
        "layman",
        re_compile_ci(r'^apt($|_[^\s]*$)'),
        re_compile_ci(r'^cl_($|_[^\s]*$)'),
        re_compile_ci(r'^selinux($|_[^\s]*$)'),
        "lxc_container",
        "pam_limits",
    )

    INDICATING_FACTS_ANY = (
        {"ansible_system": "Linux"},
    )


@Registry.register_target
class SolarisTarget(UnixLikeOperatingSystemTarget):
    """
    Represents a Solaris operating system.
    """

    FRIENDLY_NAME = "Solaris"

    INDICATING_MODULES_ANY = (
        "beadm",
        re_compile_ci(r'^dladm($|_[^\s]*$)'),
        "flowadm",
        re_compile_ci(r'^ipadm($|_[^\s]*$)'),
        re_compile_ci(r'^pkg5($|_[^\s]*$)'),
        "pkgutil",
        "svr4pkg",
    )

    INDICATING_FACTS_ANY = (
        {"os_family": "Solaris"},
    )
