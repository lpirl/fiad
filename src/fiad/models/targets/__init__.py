"""
This module contains the base implementations for all targets.

Sub modules contain the target classes themselves.
"""

from abc import ABCMeta
import logging
from itertools import product
from functools import lru_cache

from fiad.models import AbstractBaseModel
from fiad.utils import re_compile_ci


# where to import ``_sre.SRE_Pattern`` from!?
COMPILED_REGEXP_CLS = type(re_compile_ci(r''))


class AbstractBaseTarget(AbstractBaseModel, metaclass=ABCMeta):
    """
    All targets must inherit from this base class.
    """

    INDICATING_MODULES_ANY = set()
    """
    Iterable of strings or compiled regular expressions (or both mixed)
    of Ansible modules that indicate the existence of this target.
    """

    INDICATING_COMMANDS_ANY = set()
    """
    Iterable of strings or compiled regular expressions (or both mixed)
    of (shell) commands that indicate the existence of this target.
    """

    INDICATING_PACKAGES_ANY = set()
    """
    Iterable of strings or compiled regular expressions (or both mixed)
    of packages (installed through apt/yum/…) that indicate the
    existence of this target.

    USE WITH CARE: a lot of packages are pulled in as dependencies and
    thus cannot be detected using this attribute.
    """

    INDICATING_FACTS_ANY = set()
    """
    Iterable of dictionaries of facts packages that indicate the
    existence of this target.

    As the name suggests, if *any* of the dictionaries is found in *any*
    of the host facts, this target will be considered detected.
    A dictionary is considered found within host facts if *all*
    key-value pairs are found within those facts.
    """

    @classmethod
    def get_implying_targets(cls):
        """
        Get the set of targets that imply the presence of this target.

        A returned empty set means that this target was not implied by
        any other target (so far).


        ** Why is this not a class property? **

        Because we do not want to consider our super class'
        implying targets (e.g. just because "Windows" implies "Operating
        System" and "Linux" is an "Operating System" as well, "Linux"
        is not implied by "Windows", right?)


        ** details / design notes **

        The existence of this function actually smells like a design
        flaw.

        It is a consequence of implementing everything (i.e. all targets,
        in this case) at class- instead of at instance-level.

        Problem: Suppose we have these target classes::

                        .----------------.
                     .->| VirtualMachine |<-.
                     |  '----------------'  |
                     |                      |
                .--------.               .------.
                | VMWare |               | Qemu |
                '--------'               '------'

        We define the class attribute ``IMPLYING_TARGETS`` at
        ``VirtualMachine``.

        ``Qemu`` gets detected and is added to
        ``VirtualMachine.IMPLYING_TARGETS``.

        Now, accessing ``VMWare.IMPLYING_TARGETS`` would actually
        access ``VirtualMachine.IMPLYING_TARGETS`` since ``VMWare`` as no
        ``IMPLYING_TARGETS`` in its class definition and
        ``IMPLYING_TARGETS`` is hence searched up the class hierarchy.

        As a result (and this is the problematic point), ``VMWare``
        appears to be implied through ``Qemu`` because of
        ``VirtualMachine.IMPLYING_TARGETS``.

        May be we have to change the design back to using
        instances for targets and injectors (maybe as singletons?).

        For now, we fix this problem with this getter that avoids
        accessing out superclass' implying targets.
        """
        if "_implying_targets" not in cls.__dict__:
            cls._implying_targets = set()
        return cls._implying_targets

    @classmethod
    def _match_strings_or_regexps(cls, criteria, candidates):
        """
        Takes an iterable of criteria and an iterable of candidates and
        returns ``True`` (or something that evaluates to ``True``) upon
        the first match. Returns ``False`` otherwise.
        """
        for criterion, candidate in product(criteria, candidates):
            match = False
            if isinstance(criterion, str):
                match = criterion == candidate
            elif isinstance(criterion, COMPILED_REGEXP_CLS):
                match = criterion.match(candidate)
            else:
                raise TypeError(
                    "%s is neither a string nor a compiled regular "
                    "expression" % criterion
                )
            if match:
                logging.info("the use of %s indicates the presence of %s",
                             candidate, cls)
                return True

        return False

    @classmethod
    def _match_dicts(cls, criteria, candidates):
        """
        Takes an iterable of criteria (dictionaries) and an iterable of
        candidates (dictionaries as well) and returns ``True`` (or
        something that evaluates to ``True``) if *all* key-value pairs
        of *any* of the criteria dictionaries are present in *any* of
        the candidate dictionaries.
        """
        for candidate_key, candidate_value in candidates.items():
            for criterion in  criteria:
                if candidate_value.contains(criterion):
                    return "found facts %s in facts of %s" % (criterion,
                                                              candidate_key)

        return False

    @classmethod
    def detect_by_indicators(cls, playbook):
        """
        Evaluates ``INDICATING_*_ANY`` and returns something that
        evaluates to ``True`` upon the first match.
        """

        # check modules used:
        module_criteria = cls.INDICATING_MODULES_ANY
        module_names = playbook.get_module_names()
        match = cls._match_strings_or_regexps(module_criteria,
                                              module_names)
        if match:
            return match

        # check commands used:
        command_criteria = cls.INDICATING_COMMANDS_ANY
        command_names = playbook.get_command_names()
        match = cls._match_strings_or_regexps(command_criteria,
                                              command_names)
        if match:
            return match

        # check packages used:
        package_criteria = cls.INDICATING_PACKAGES_ANY
        package_names = playbook.get_package_names()
        match = cls._match_strings_or_regexps(package_criteria,
                                              package_names)
        if match:
            return match

        # check facts:
        facts_criteria = cls.INDICATING_FACTS_ANY
        hosts_facts = playbook.get_facts()
        if hosts_facts:
            match = cls._match_dicts(facts_criteria, hosts_facts)
            if match:
                return match

        return False

    @classmethod
    def detect(cls, playbook):
        """
        Detects if this target is present in ``playbook``.
        """
        return cls.detect_by_indicators(playbook)

    @classmethod
    def get_implied_targets(cls):
        """
        Returns superclasses of ``cls`` (recursively) that are targets.

        Due to possible multiple inheritance the return value is
        (currently) a set (to keep it simple).

        Of course, abstract classes will be ignored and the recursion
        is stopped when reaching ``AbstractBaseTarget``.
        """
        logging.debug("searching implied targets for %s", cls)

        implied_targets = set()

        for implied_target in cls.__mro__[1:]:

            # checks are roughly ordered by cost, ascending:

            # defensive safety check
            if implied_target is None:
                logging.warning(("The superclass of %s is None which "
                                 "should never happen, really. "
                                 "Please report this issue."),
                                implied_target)
                continue

            # check if we reached the root of our class hierarchy of
            # targets
            if implied_target is AbstractBaseTarget:
                continue

            # verify being in correct branch of the class hierarchy
            if AbstractBaseTarget not in implied_target.__mro__:
                continue

            if implied_target in implied_targets:
                continue

            logging.debug("found target implication %s -> %s",
                          cls, implied_target)

            implied_target.get_implying_targets().add(cls)

            implied_targets.add(implied_target)
            implied_targets.update(
                implied_target.get_implied_targets()
            )

        return implied_targets

    @classmethod
    def verbose_str(cls):
        """
        Returns the string representation of this class' type plus the
        ones of all implied targets, if any.
        """
        output = str(cls)
        implying_targets = cls.get_implying_targets()
        if implying_targets:
            names = (str(t) for t in implying_targets)
            names_string = ", ".join(names)
            output += " (implied through %s)" % names_string
        return output
