"""
This module contains targets which represent (source or intermediate)
code.

Currently, these targets exists largely for the definition of some
targets but, sadly, lack the definitions/logic to be detected.
"""
from fiad.models.targets import AbstractBaseTarget
from fiad.models import Registry


@Registry.register_target
class SourceCodeTarget(AbstractBaseTarget):
    """
    Represents the availability of source code.
    """
    INDICATING_MODULES_ANY = ("make")


@Registry.register_target
class IntermediateCodeTarget(AbstractBaseTarget):
    """
    Represents the availability of an intermediate code representation,
    which may be interpreted.
    """


@Registry.register_target
class BytecodeTarget(IntermediateCodeTarget):
    """
    Represents the availability of bytecode.
    """


@Registry.register_target
class SymbolTarget(IntermediateCodeTarget):
    """
    Represents the availability of (debug) symbols.
    """


@Registry.register_target
class BinaryTarget(AbstractBaseTarget):
    """
    Represents the availability of binaries.
    """


@Registry.register_target
class DLLTarget(BinaryTarget):
    """
    Represents the availability of dynamically linked libraries.
    """
