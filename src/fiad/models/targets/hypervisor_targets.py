"""
This module contains the different hypervisor targets.
Superordinate classes might be part of this module as well.
"""

from fiad.models.targets import AbstractBaseTarget
from fiad.models import Registry
from fiad.utils import re_compile_ci


class HypervisorTarget(AbstractBaseTarget):
    """
    Anything that indicates that there is control over virtual machines.

    Administrative privileges *inside* a virtual machine do *not*
    fulfill this requirement.
    """

    FRIENDLY_NAME = "hypervisor"


@Registry.register_target
class VMWareTarget(HypervisorTarget):
    """
    Anything that indicates that there is control over a VMWare
    installation.
    """

    FRIENDLY_NAME = "VMWare"

    INDICATING_MODULES_ANY = (
        re_compile_ci(r'^vmware_[^\s]*$'),
        re_compile_ci(r'^vsphere_[^\s]*$'),
    )

    INDICATING_COMMANDS_ANY = (
        "esxcli",
        re_compile_ci(r'^vicfg[-_][^\s]*$'),
        re_compile_ci(r'^vmware[-_]cmd$'),
        "vifs",
        "vmkfstools",
        "vihostupdate",
        "vapi",
        "vcenter",
    )


@Registry.register_target
class LibVirtTarget(HypervisorTarget):
    """
    Anything that indicates the use of libvirt.
    """

    FRIENDLY_NAME = "libvirt"

    INDICATING_MODULES_ANY = (
        re_compile_ci(r'^virt($|_[^\s]*$)'),
    )

    INDICATING_COMMANDS_ANY = (
        "virsh",
        re_compile_ci(r'^virt[-_][^\s]*$'),
    )


@Registry.register_target
class DockerTarget(HypervisorTarget):
    """
    Anything that indicates the use of Docker.
    """

    FRIENDLY_NAME = "Docker"

    INDICATING_MODULES_ANY = (
        re_compile_ci(r'^docker($|_[^\s]*$)'),
    )

    INDICATING_COMMANDS_ANY = (
        re_compile_ci(r'^docker($|[-_][^\s]*$)'),
    )


@Registry.register_target
class VirtualBoxTarget(HypervisorTarget):
    """
    Anything that indicates that there is control over a VirtualBox
    installation.
    """

    FRIENDLY_NAME = "VirtualBox"

    INDICATING_COMMANDS_ANY = (
        "virtualbox",
        "vboxmanage",
        "vboxheadless",
        "vboxballoonctrl",
    )


@Registry.register_target
class QemuTarget(HypervisorTarget):
    """
    Anything that indicates the use of Qemu.
    """

    FRIENDLY_NAME = "Qemu"

    INDICATING_COMMANDS_ANY = (
        re_compile_ci(r'^qemu($|[-_][^\s]*$)'),
    )
