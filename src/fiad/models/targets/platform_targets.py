"""
This module contains targets representing hardware platforms.
"""
from fiad.models import Registry
from fiad.models.targets import AbstractBaseTarget


class PlatformTarget(AbstractBaseTarget):
    """
    Represents a hardware platform.
    """

    FRIENDLY_NAME = "platform"


@Registry.register_target
class GPUTarget(PlatformTarget):
    """
    Represents a (GP)GPU.
    """

    FRIENDLY_NAME = "GPU"

    # TODO: how to detect this?


@Registry.register_target
class X86Target(PlatformTarget):
    """
    Represents an x86 architecture.
    """

    FRIENDLY_NAME = "x86"

    INDICATING_FACTS_ANY = (
        {"ansible_architecture": "x86"},
    )


@Registry.register_target
class Amd64Target(X86Target):
    """
    Represents an AMD64 architecture.
    """

    FRIENDLY_NAME = "amd64"

    INDICATING_FACTS_ANY = (
        {"ansible_architecture": "x86_64"},
    )


@Registry.register_target
class Power64Target(PlatformTarget):
    """
    Represents a PowerPC architecture.
    """

    FRIENDLY_NAME = "Power64"

    INDICATING_FACTS_ANY = (
        {"ansible_architecture": "ppc64"},
    )


@Registry.register_target
class Sparc64Target(PlatformTarget):
    """
    Represents a Sparc architecture.
    """

    FRIENDLY_NAME = "Sparc64"

    INDICATING_FACTS_ANY = (
        {"ansible_architecture": "sparc64"},
    )


# TODO: to be continued (e.g., ARM)
