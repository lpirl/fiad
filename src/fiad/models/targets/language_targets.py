"""
See :py:class:`LanguageTarget`.
"""
from fiad.models.targets import AbstractBaseTarget
from fiad.models import Registry
from fiad.utils import re_compile_ci


class LanguageTarget(AbstractBaseTarget):
    """
    Represents a Programming Language.
    """


@Registry.register_target
class LLVMTarget(AbstractBaseTarget):
    """
    Mixin-like target representing programming languages supported by
    LLVM.
    """


@Registry.register_target
class JavaTarget(LanguageTarget, LLVMTarget):
    """
    Represents the Java programming Language.
    """

    INDICATING_PACKAGES_ANY = (
        re_compile_ci(r'^jdk($|[-_][^\s]*$)'),
        re_compile_ci(r'^java($|[-_][^\s]*$)'),
    )


@Registry.register_target
class CTarget(LanguageTarget, LLVMTarget):
    """
    Represents the C programming Language.
    """

    # TODO: INDICATING_COMMANDS_ANY = ()

    # TODO: can probably be extended:
    INDICATING_PACKAGES_ANY = ("gcc", "build-essential")


@Registry.register_target
class CppTarget(LanguageTarget, LLVMTarget):
    """
    Represents the C++ programming Language.
    """
    IS_INTERPRETED = False

    # TODO: INDICATING_COMMANDS_ANY = ()

    # TODO: can probably be extended:
    INDICATING_PACKAGES_ANY = ("gcc-c++")


@Registry.register_target
class PythonTarget(LanguageTarget, LLVMTarget):
    """
    Represents the Python programming Language.
    """
    IS_INTERPRETED = True

    INDICATING_MODULES_ANY = ("pip", "easy_install")

    INDICATING_PACKAGES_ANY = (
        re_compile_ci(r'^python($|[-_][^\s]*$)'),
    )

    INDICATING_COMMANDS_ANY = (
        re_compile_ci(r'^(.*/)?python([0-9])?'),
    )


@Registry.register_target
class AdaTarget(LanguageTarget):
    """
    Represents the Ada Programming Language.
    """
    IS_INTERPRETED = False

    # TODO: how to detect?


@Registry.register_target
class CUDATarget(LanguageTarget, LLVMTarget):
    """
    Represents CUDA programming language.
    """
    IS_INTERPRETED = False

    INDICATING_PACKAGES_ANY = ("cuda")

    INDICATING_COMMANDS_ANY = (
        'nvcc',
    )
