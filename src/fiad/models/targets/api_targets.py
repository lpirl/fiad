"""
See :py:class:`APITarget`.
"""
from fiad.models.targets import AbstractBaseTarget
from fiad.models import Registry


@Registry.register_target
class APITarget(AbstractBaseTarget):
    """
    Represents an API.
    """
    # TODO: how to detect the usage of libraries? linkage commands etc?
    # LP: we can't? :( Except maybe libraries explicitly installed...


@Registry.register_target
class POSIXTarget(AbstractBaseTarget):
    """
    Anything that indicates the use of the POSIX API.

    This explicitly excludes Microsoft Windows (for now) due to the
    limited POSIX support.
    """

    # used by and implied through operating system targets

    FRIENDLY_NAME = "POSIX API"
