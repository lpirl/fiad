"""
This module contains the different virtual machine targets – one target
per virtualization tool.
Superordinate classes of virtualization tools or techniques might be part
of this module as well.
"""

from fiad.models.targets import AbstractBaseTarget
from fiad.models import Registry
from fiad.utils import re_compile_ci


class VirtualMachineTarget(AbstractBaseTarget):
    """
    A virtual machine – independent of the technique and hypervisor used.
    """

    FRIENDLY_NAME = "virtual machine"


class CloudVirtualMachineTarget(VirtualMachineTarget):
    """
    Virtual machines running on IaaS AKA cloud resources.
    """

    FRIENDLY_NAME = "virtual machine in the Cloud"


@Registry.register_target
class EC2NodeTarget(CloudVirtualMachineTarget):
    """
    Virtual machines running on Amazon EC2.
    """

    FRIENDLY_NAME = "EC2 virtual machine"

    INDICATING_MODULES_ANY = (
        re_compile_ci(r'^ec2($|_[^\s]*$)'),
    )


@Registry.register_target
class AzureTarget(CloudVirtualMachineTarget):
    """
    Virtual machines running on Microsoft Azure.
    """

    FRIENDLY_NAME = "Azure virtual machine"

    INDICATING_MODULES_ANY = (
        re_compile_ci(r'^azure($|_[^\s]*$)'),
    )

    INDICATING_COMMANDS_ANY = (
        "azure",
    )


@Registry.register_target
class VMwareTarget(CloudVirtualMachineTarget):
    """
    VMware virtual machines.
    """

    FRIENDLY_NAME = "VMware virtual machine"

    INDICATING_MODULES_ANY = (
        re_compile_ci(r'^vmware_guest($|_[^\s]*$)'),
        re_compile_ci(r'^vsphere_guest($|_[^\s]*$)'),
    )
