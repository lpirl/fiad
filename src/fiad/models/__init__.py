# encoding: utf8

"""
This module contains the base implementations for all kinds of models
(i.e.: injectors, rules, targets) as well as the model registry.

Sub modules contain the models itself.

All models classes are never instantiated but used with their staitc
behavior and state (i.e. treated as singletons).
That way, they can (statically)  refer to each other using only their class.
As a result, a lot of matching logic is simplified.
Once we need multiple instances of a model, we have to revise this design
decision (maybe use the class as a registry for instances etc.).
"""

from pkgutil import walk_packages
from importlib import import_module
import logging
import abc


class Registry:
    """
    Models register at this class.

    This class cannot be instantiated since we use class methods and
    properties to act like a singleton.
    """

    targets = set()
    injectors = set()

    def __init__(self):
        """
        Class can not be instantiated.
        Please use it's classmethods.
        """
        raise RuntimeError(self.__init__.__doc__)

    @classmethod
    def _register(cls, registry, model_cls):
        registry.add(model_cls)
        return model_cls

    @classmethod
    def register_target(cls, model_cls):
        """
        Registers a target class.
        """
        return cls._register(cls.targets, model_cls)

    @classmethod
    def register_injector(cls, model_cls):
        """
        Registers a injector class.
        """
        return cls._register(cls.injectors, model_cls)


class AbstractModelType(abc.ABCMeta):
    """
    A type just like the supertype but it converts to string with
    ``FRIENDLY_NAME`` if possible.
    """
    def __str__(cls):
        return (cls.FRIENDLY_NAME or
                super(AbstractModelType, cls).__name__)


# This class is not used by itself but used as a base class for the
# base model classes. We hence:
# pylint: disable=too-few-public-methods
class AbstractBaseModel(metaclass=AbstractModelType):
    """
    Provides common functionality for models.

    All class properties might be ``None`` (read: models should expect
    them to be).

    The docstring of a model serves as a detailed description.

    Be careful when defining class attributes: those must be valid
    for all possible subclasses (because of the semantics of inheritance).
    """

    FRIENDLY_NAME = None
    """
    Friendly name of the injector.
    """

    @classmethod
    def set_up(cls, arg_parser):
        """
        Called after initialization (models must not override
        :py:func:`__init__`).
        """


def import_submodules(package, recursive=True):
    """
    Import all submodules of a module, recursively, including subpackages.

    :param package: package (name or actual module)
    :type package: str | module
    :rtype: dict[str, types.ModuleType]

    Thanks to http://stackoverflow.com/a/25562415
    """
    if isinstance(package, str):
        package = import_module(package)
    results = {}
    for _, name, is_pkg in walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        results[full_name] = import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results

import_submodules(__name__)
