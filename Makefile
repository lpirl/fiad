HIERARCHY_IMG ?= doc/source/hierarchy.svg
PLAYBOOKS_ROOT = src/test/playbooks
FIAD ?= ./fiad

.PHONY: help
help:
	@echo "targets:"
	@echo
	@echo "  help       you are reading this already"
	@echo
	@echo "  examples   run some examples to get an idea how this works"
	@echo
	@echo "  test       runs the few tests we have"
	@echo
	@echo "  pylint     runs code linting"
	@echo
	@echo "  ci         makes targets 'test' and 'pyling'"
	@echo
	@echo "  hierarchy  generates a nice image showing the hierarchy of our"
	@echo "             models (injectors and targets) under"
	@echo
	@echo "  doc        generates code documentation"
	@echo
	@echo "  clean      removes generated files (e.g., Python object files"
	@echo "             docs, hierarchy image)"

.PHONY: test
test:
	python3 -m unittest discover -vp "*_test.py" src

lint:
	pylint --exit-zero src/fiad

hierarchy: $(HIERARCHY_IMG)
$(HIERARCHY_IMG):

	@type pyreverse > /dev/null || (\
		echo "Please pip install . doc" && exit 1 \
	)
	PYTHONPATH=src/fiad pyreverse -ko dot src/fiad

	sed -i "s/rankdir=BT/rankdir=RL\noverlap=false/" classes.dot

	@type dot > /dev/null || (\
		echo "Please (apt-get/aptitude/yum/…) install graphviz" && exit 1 \
	)
	dot -Tsvg classes.dot > $@

	rm -f classes.dot packages.dot

.PHONY: doc
doc: $(HIERARCHY_IMG)
	@type sphinx-apidoc > /dev/null || (\
		echo "Please pip install sphinx" && exit 1 \
	)
	PYTHONPATH=src/fiad sphinx-apidoc -o doc/source/apidoc -M src/fiad
	$(MAKE) -C doc html -e

clean:
	find \( \
		-name __pycache__ -or \
		-name '*.pyo' -or \
		-name '*.pyc' \
	\) -delete
	rm -rf \
		$(HIERARCHY_IMG) \
		doc/source/apidoc \
		doc/build

examples:
	@# windows target & crashme
	@echo -n "\n$$ "
	$(FIAD) $(PLAYBOOKS_ROOT)/ansible-examples/windows/run-powershell.yml

	@# ec2 & chaos monkey
	@echo -n "\n$$ "
	$(FIAD) --recursive $(PLAYBOOKS_ROOT)/ansible-ec2-hadoop-cluster

	@# vSphere & iofuzz
	@echo -n "\n$$ "
	$(FIAD) $(PLAYBOOKS_ROOT)/ansible-vmware/create_vms.yml

	@# VirtualBox & iofuzz
	@echo -n "\n$$ "
	$(FIAD) $(PLAYBOOKS_ROOT)/kaybus_challenge/vbox_full_stack.yml

	@# Docker & iofuzz
	@echo -n "\n$$ "
	$(FIAD) $(PLAYBOOKS_ROOT)/microwizard/provisioning/microwizard.yml

	@# libvirt & iofuzz
	@echo -n "\n$$ "
	$(FIAD) $(PLAYBOOKS_ROOT)/ansible_vmcreator/makevm.yml

	@# localhost w/ Ansible facts
	@echo -n "\n$$ "
	$(FIAD) --facts $(PLAYBOOKS_ROOT)/localhost.yml

	@echo
