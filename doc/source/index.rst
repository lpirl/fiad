.. fiad documentation master file, created by
   sphinx-quickstart on Tue Apr 26 23:04:30 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fiad's documentation!
================================

.. toctree::
  :maxdepth: 10

  ./apidoc/modules.rst

Complete class hierarchy
------------------------

.. image:: hierarchy.svg
   :width: 750px
   :align: center
