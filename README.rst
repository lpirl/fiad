Fault Injection Advisor
=======================

The aim of the Fault Injection ADvisor (FIAD) is to extract applicable
fault injection tools from provisioning configuration files (currently
only Ansible).

trivia
------

Distributed software systems are hard to understand, especially their
increasingly critical dependability properties.

Fault injection is an acknowledged approach to experimentally assess the
fault tolerance of complex software. However,

* it is hard to find the suitable tool for fault injection
  from the broad and unstructured projects, research and products
  available; and
* it is hard to find suitable locations and a fault model for fault
  injection.

Since complex infrastructures are increasingly configured using
provisioning tools, we explore an approach to automatically extract
suitable fault injection targets and tools from provisioning
configuration files.

To achieve the aforementioned goal, FIAD also structures existing
fault injection tools in its source code. As a by-product, this
structured capturing has the great potential to derive a classification
of existing fault injection tools.

FIAD detects *targets* based on, e.g., commands, modules, or facts used
in the Ansible configuration files.
For *fault injectors* to be suggested, a certain combinations of the
aforementioned targets is required.
Using this architecture, FIAD can suggest fault injection tools
applicable to the given infrastructure
(e.g., the Windows target is detected because the ``win_acl`` module is
used, the fault injector ``crashme`` requires Windows,
hence FIAD suggest ``crashme`` as an applicable fault injection tool).

Particularly, this first proof of concept of the Fault Injection Advisor
includes information about a handful of fault injection tools, a few
dozens of fault injection targets, and works with Ansible Playbooks.
It can thus be used, e.g., by software engineers to identify tools for
fault injection assessments.
The architecture, of course, is designed with the intention to be easily
extensible, (i.e., include further fault injection targets and tools).

For further information, see also

* L. Pirl, L. Feinbube, and A. Polze, "Structuring Software Fault
  Injection Tools for Programmatic Evaluation," in Proceedings of The
  Ninth International Conferences on Advanced Service Computing,
  Athens, Greece, 2017, pp. 3–6 [Online]. Available:
  https://pdfs.semanticscholar.org/bf74/0791a8eca2141306d58fed5ca32df267d6f1.pdf

using FIAD
----------

* optionally, but recommended:
  setup a development environment using ``virtualenvwrapper``:

  #. create virtualenv: ``$ mkvirtualenv -p $(type python3 | cut -d " " -f3) fiad``

     * (automatically enters the environment)

  #. enter virtualenv: ``$ workon fiad``
  #. leave virtualenv: ``$ deactivate``

* clone this repository:
  ``$ git clone --recursive https://gitlab.com/lpirl/fiad.git``
* ``$ cd fiad``
* install dependencies: ``$ pip3 install .``
* run: ``$ ./fiad /path/to/your/playbook.yml``

If you have no Playbook at hand, you can run:

* ``$ make examples``

development / documentation
---------------------------

If you want to extend FIAD, for example, you might want to browse the
`documentation <https://lpirl.gitlab.io/fiad/>`__.

Enhancing and extending the documentation is as welcome as enhancing and
extending the code.

.. image:: https://gitlab.com/lpirl/fiad/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/fiad/pipelines

future work / ideas
-------------------

* plot fault injectors in a nice way

  * ideally in something like a classification

* leverage the probably existing/configured shell access to the
  infrastructure to obtain more information
* *targets* could have relationships to reflect system structure

  * e.g.: "EC2 instance -> has disk, CPU, …"
    (i.e. implications)
  * e.g.: "EC2 w/ network -> software defined networking"
    (i.e. abstractions)
  * partly possible with class hierarchy of targets

    * But is this enough?

  * Impractical because too many exceptions from the rules?

* more tests (of course…)
* a lot more

contributors
------------

* Lena Feinbube
* Lukas Pirl
